package org.nostra.kumuluz.resources;

import org.nostra.kumuluz.models.Students;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * agus w on 3/15/16.
 */
@Path("/students")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class StudentsResource {

    @PersistenceContext(unitName = "schools")
    private EntityManager em;

    @GET
    public Response getStudents() {

        TypedQuery<Students> query = em.createNamedQuery("Students.findAll", Students.class);

        List<Students> studentses = query.getResultList();

        return Response.ok(studentses).build();
    }

    @GET
    @Path("/{id}")
    public Response getStudents(@PathParam("id") String id) {

        Students b = em.find(Students.class, id);

        return Response.ok(b).build();
    }

    @POST
    public Response createStudents(Students b) {

        b.setId(null);

        em.getTransaction().begin();

        em.persist(b);

        em.getTransaction().commit();

        return Response.status(Response.Status.CREATED).entity(b).build();
    }
}
