package org.nostra.kumuluz;

import javax.ws.rs.ApplicationPath;

/**
 * Blog App
 */
@ApplicationPath("/v1/")
public class App extends javax.ws.rs.core.Application {
}
