package org.nostra.kumuluz.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import java.util.Date;
import java.util.UUID;

/**
 * agus w on 3/15/16.
 */
@Data
@Entity
@NamedQuery(name="Students.findAll", query="SELECT s FROM Students s")
public class Students {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private Date dob;

    @PrePersist
    public void prePersist(){
        id = UUID.randomUUID().toString();
    }
}
